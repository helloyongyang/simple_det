import torch.nn as nn
from simple_det.model.component.conv_module import ConvActModule


class RetinaHead(nn.Module):
    def __init__(
        self,
        num_classes,
        num_feats,
        in_channels,
        feat_channels,
        stacked_convs,
        num_base_priors,
    ):
        super(RetinaHead, self).__init__()

        self.num_classes = num_classes
        self.num_feats = num_feats
        self.in_channels = in_channels
        self.feat_channels = feat_channels
        self.stacked_convs = stacked_convs
        self.num_base_priors = num_base_priors

        self.cls_convs = nn.ModuleList()
        self.reg_convs = nn.ModuleList()
        for i in range(self.stacked_convs):
            chn = self.in_channels if i == 0 else self.feat_channels
            self.cls_convs.append(
                ConvActModule(
                    chn,
                    self.feat_channels,
                    3,
                    stride=1,
                    padding=1))
            self.reg_convs.append(
                ConvActModule(
                    chn,
                    self.feat_channels,
                    3,
                    stride=1,
                    padding=1))
        self.retina_cls = nn.Conv2d(
            self.feat_channels,
            self.num_base_priors * self.num_classes,
            3,
            padding=1)
        self.retina_reg = nn.Conv2d(
            self.feat_channels, self.num_base_priors * 4, 3, padding=1)

    def forward(self, feats):
        cls_score = []
        bbox_pred = []
        for i in range(self.num_feats):
            x = feats[i]
            cls_feat = x
            reg_feat = x
            for cls_conv in self.cls_convs:
                cls_feat = cls_conv(cls_feat)
            for reg_conv in self.reg_convs:
                reg_feat = reg_conv(reg_feat)
            cls_score.append(self.retina_cls(cls_feat))
            bbox_pred.append(self.retina_reg(reg_feat))
        return cls_score, bbox_pred


if __name__ == "__main__":
    import torch

    net = RetinaHead(num_classes=80,
                     num_feats=5,
                     in_channels=256,
                     feat_channels=256,
                     stacked_convs=4,
                     num_base_priors=9)

    print(net)

    x = [torch.randn(1, 256, 64, 64),
         torch.randn(1, 256, 32, 32),
         torch.randn(1, 256, 16, 16),
         torch.randn(1, 256, 8, 8),
         torch.randn(1, 256, 4, 4),]

    cls_score, bbox_pred = net(x)

    for i in range(len(cls_score)):
        print(i, cls_score[i].shape)

    for i in range(len(bbox_pred)):
        print(i, bbox_pred[i].shape)
