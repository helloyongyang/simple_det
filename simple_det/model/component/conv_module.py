import torch.nn as nn
from .activation import act_layers


class ConvActModule(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        stride=1,
        padding=0,
        dilation=1,
        groups=1,
        bias=True,
        activation="ReLU",
    ):
        super(ConvActModule, self).__init__()

        self.conv = nn.Conv2d(
            in_channels,
            out_channels,
            kernel_size,
            stride=stride,
            padding=padding,
            dilation=dilation,
            groups=groups,
            bias=bias,
        )

        self.act = act_layers(activation)
        self.out_channels = out_channels

    def forward(self, x):
        x = self.conv(x)
        x = self.act(x)
        return x
