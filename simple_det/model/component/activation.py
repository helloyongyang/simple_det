import torch.nn as nn


def act_layers(name):
    if name == "ReLU":
        return nn.ReLU(inplace=True)
    else:
        return None
