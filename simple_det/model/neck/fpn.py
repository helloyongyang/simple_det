import torch.nn as nn
import torch.nn.functional as F


class FPN(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        num_outs,
        add_extra_convs=False
    ):
        super(FPN, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.num_outs = num_outs
        self.add_extra_convs = add_extra_convs
        self.in_channels_len = len(self.in_channels)

        self.lateral_convs = nn.ModuleList()
        self.fpn_convs = nn.ModuleList()
        for i in range(self.in_channels_len):
            l_conv = nn.Conv2d(
                self.in_channels[i],
                out_channels,
                kernel_size=1
            )

            fpn_conv = nn.Conv2d(
                out_channels,
                out_channels,
                kernel_size=3,
                padding=1
            )

            self.lateral_convs.append(l_conv)
            self.fpn_convs.append(fpn_conv)

        self.extra_levels = self.num_outs - self.in_channels_len
        if self.add_extra_convs and self.extra_levels > 0:
            for i in range(self.extra_levels):
                if i == 0 and self.add_extra_convs == 'on_input':
                    in_channels_extra_convs = self.in_channels[-1]
                else:
                    in_channels_extra_convs = out_channels
                extra_fpn_conv = nn.Conv2d(
                    in_channels_extra_convs,
                    out_channels,
                    kernel_size=3,
                    stride=2,
                    padding=1)
                self.fpn_convs.append(extra_fpn_conv)

    def forward(self, inputs):
        laterals = []
        for i in range(self.in_channels_len):
            laterals.append(
                self.lateral_convs[i](inputs[i])
            )

        for i in range(self.in_channels_len - 1, 0, -1):
            laterals[i - 1] += F.interpolate(
                laterals[i], scale_factor=2, mode="nearest"
            )

        outs = [
            self.fpn_convs[i](laterals[i]) for i in range(self.in_channels_len)
        ]

        if self.extra_levels > 0:
            if not self.add_extra_convs:
                for i in range(self.extra_levels):
                    outs.append(F.max_pool2d(outs[-1], 1, stride=2))
            else:
                if self.add_extra_convs == 'on_input':
                    extra_source = inputs[-1]
                elif self.add_extra_convs == 'on_lateral':
                    extra_source = laterals[-1]
                elif self.add_extra_convs == 'on_output':
                    extra_source = outs[-1]
                else:
                    raise NotImplementedError
                outs.append(
                    self.fpn_convs[self.in_channels_len](extra_source))
                for i in range(self.in_channels_len + 1, self.num_outs):
                    outs.append(self.fpn_convs[i](outs[-1]))

        return outs


if __name__ == "__main__":
    import torch

    in_channels = [128, 256, 512]
    out_channels = 256
    add_extra_convs = "on_output"
    net = FPN(in_channels=in_channels, out_channels=out_channels,
              num_outs=5, add_extra_convs=add_extra_convs)

    print(net)

    x = [torch.randn(1, 128, 64, 64),
         torch.randn(1, 256, 32, 32),
         torch.randn(1, 512, 16, 16)]

    y = net(x)

    for i in range(len(y)):
        print(i, y[i].shape)
