import torch.nn as nn
from .network_helper import NetworkHelper


class ModelHelper(nn.Module):
    def __init__(self, backbone, neck, head, postprocess):
        super(ModelHelper, self).__init__()
        self.network = NetworkHelper(backbone, neck, head)
        self.postprocess = postprocess
        self.mode = "train"

    def set_mode(self, mode):
        assert mode in ["train", "eval", "inference", "export", "custom"]
        self.mode = mode
        if self.mode in ["train", "custom"]:
            self.network.train()
        else:
            self.network.eval()

    def forward(self, x, label=None):
        if self.mode == "train":
            return self.forward_train(x, label)
        if self.mode == "eval":
            return self.forward_eval(x, label)
        if self.mode == "inference":
            return self.forward_inference(x)
        if self.mode == "export":
            return self.forward_export(x)
        if self.mode == "custom":
            return self.forward_custom(x)

    def forward_train(self, x, label):
        pass

    def forward_eval(self, x, label):
        pass

    def forward_inference(self, x):
        pass

    def forward_export(self, x):
        pass

    def forward_custom(self, x):
        x = self.network(x)
        return x
