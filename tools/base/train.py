import torch

from configs.retinanet.retinanet_r18 import config_retinanet_r18
from simple_det.helper.model_helper import ModelHelper


config = config_retinanet_r18()
model = ModelHelper(
    backbone=config.backbone,
    neck=config.neck,
    head=config.head,
    postprocess=config.postprocess
)

model.set_mode("custom")

print(model)


x = torch.randn(1, 3, 512, 512)
cls_score, bbox_pred = model(x)

for i in range(len(cls_score)):
    print(i, cls_score[i].shape)

for i in range(len(bbox_pred)):
    print(i, bbox_pred[i].shape)


dataset = config.dataset

for img, target, img_info, img_id in dataset:
    print("img : ", img)
    print("img.shape : ", img.shape)
    print("target : ", target)
    print("img_info : ", img_info)
    print("img_id : ", img_id)
