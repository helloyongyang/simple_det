from configs._base_.base import config_base
from simple_det.model.backbone.resnet import resnet18
from simple_det.model.neck.fpn import FPN
from simple_det.model.head.RetinaHead import RetinaHead
from simple_det.data.coco import COCODataset


class config_retinanet_r18(config_base):
    def __init__(self):
        super(config_retinanet_r18, self).__init__()
        self.backbone = resnet18(
            out_stages=[2, 3, 4]
        )
        self.neck = FPN(
            in_channels=[128, 256, 512],
            out_channels=256,
            num_outs=5,
            add_extra_convs="on_input"
        )
        self.head = RetinaHead(
            num_classes=80,
            num_feats=5,
            in_channels=256,
            feat_channels=256,
            stacked_convs=4,
            num_base_priors=9
        )
        self.postprocess = None

        self.dataset = COCODataset(
            data_dir="/mnt/data/coco",
            json_file="instances_train2017.json",
            name="train2017",
            preproc=None,
        )
